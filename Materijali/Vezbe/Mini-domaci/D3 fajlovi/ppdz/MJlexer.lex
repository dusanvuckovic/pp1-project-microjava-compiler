
package ppdz;
import java_cup.runtime.Symbol;


%%

%{
		// ukljucivanje informacije o poziciji tokena
		private Symbol new_symbol(int type) {
				return new Symbol(type, yyline+1, yycolumn);
		}
		// ukljucivanje informacije o poziciji tokena
		private Symbol new_symbol(int type, Object value) {
				return new Symbol(type, yyline+1, yycolumn, value);
		}
%}

%cup

%xstate COMMENT

%eofval{ 
return new_symbol(sym.EOF);
%eofval}

%line
%column

%%
" " {}
"\b" {}
"\t" {}
"\r\n" {}
"\f" {}
"new"   {return new_symbol(sym.NEW);}
"class" {return new_symbol(sym.CLASS);}
"final" {return new_symbol(sym.FINAL);}
"print" {return new_symbol(sym.PRINT);}
"return" {return new_symbol(sym.RETURN);}
"void" {return new_symbol(sym.VOID);}
"+" {return new_symbol(sym.PLUS);}
"-" {return new_symbol(sym.MINUS);}
"*" {return new_symbol(sym.TIMES);} 
"/" {return new_symbol(sym.DIV);}
"=" {return new_symbol(sym.EQUAL);}
";" {return new_symbol(sym.SEMI);}
"," {return new_symbol(sym.COMMA);}
"(" {return new_symbol(sym.LPAREN);}
")" {return new_symbol(sym.RPAREN);}
"{" {return new_symbol(sym.LBRACE);}
"}" {return new_symbol(sym.RBRACE);}
"[" {return new_symbol(sym.LSQUARE);}
"]" {return new_symbol(sym.RSQUARE);}
"//" {yybegin(COMMENT);}
<COMMENT>. {yybegin(COMMENT);}
<COMMENT>"\r\n" {yybegin(YYINITIAL);}
[0-9]+ {return new_symbol(sym.NUMBER, new Integer (yytext()));}
([a-z]|[A-Z])[a-z|A-Z|0-9|_]* {return new_symbol (sym.IDENT, yytext());}
"'"[\040-\176]"'" {return new_symbol (sym.CHARCONST, new Character (yytext().charAt(1)));}
. {System.err.println("Leksicka greska ("+yytext()+") u liniji "+(yyline+1));}