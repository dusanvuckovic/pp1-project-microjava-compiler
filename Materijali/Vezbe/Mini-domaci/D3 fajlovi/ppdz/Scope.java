/**
 * MicroJava Symbol Table Scopes
 */

package ppdz;

public class Scope {
	Scope outer;		// reference to enclosing scope
	public Obj locals;	// symbol table of this scope
	public int nVars;	// # of variables in this scope
}
