package simple;
import java_cup.runtime.Symbol;

%%

%cup

%eofval{
  return new Symbol(sym.EOF);
%eofval}

%%

" " {}
\b {}
\t {}
\r\n {}
\f {}

"+" { return new Symbol(sym.PLUS); }
"-" { return new Symbol(sym.MINUS); }
"/" { return new Symbol(sym.DIVIDE); }
"%" { return new Symbol(sym.MOD); }
"*" { return new Symbol(sym.TIMES); }
"(" { return new Symbol(sym.LPAREN); }
")" { return new Symbol(sym.RPAREN); }
";" { return new Symbol(sym.SEMI); }
[0-9]+ { return new Symbol(sym.NUMBER, new Integer(yytext()) ); }
. {}

