package Primer5;
import java_cup.runtime.*;

class LexFactory{

     public static Scanner GetLex( String filename){
       	try {
       		java.io.File fajl = new java.io.File(filename);
       		java.io.FileReader r = new java.io.FileReader(fajl);
       		return new Yylex(r);
       	}catch(Exception ex) {
       		return null;
       	}	
     }
}