package Primer5;

import java_cup.runtime.Symbol;
%%

//	%cup
%implements	java_cup.runtime.Scanner
%function	next_token
%type		java_cup.runtime.Symbol

// na kraju fajla vrati EOF
%eofval{
return new Symbol(sym.EOF);
%eofval}

%%
";"	{ return new Symbol(sym.SEMI); }
"+"	{ return new Symbol(sym.PLUS); }
"*"	{ return new Symbol(sym.TIMES); }
"("	{ return new Symbol(sym.LPAREN); }
")"	{ return new Symbol(sym.RPAREN); }
[0-9]+	{ return new Symbol(sym.NUMBER, new Integer(yytext())); }
[ \t\n]	{ /* ignorise prazna mesta */ }
.	{ System.err.println("Nelegalni karakter: "+yytext()); } 

