
package Primer5;

import org.junit.* ;
import static org.junit.Assert.* ;
import java_cup.runtime.* ;

public class LexFactoryTest {

	private Scanner lex;

	@Before
	public void SetUp() {
		lex = LexFactory.GetLex("Primer5\\Test.txt");
	}

	@Test
	public void testGetLex () throws java.lang.Exception {

		Assert.assertEquals(lex.next_token().sym, sym.NUMBER);
		Assert.assertEquals(lex.next_token().sym, sym.PLUS);
		Assert.assertEquals(lex.next_token().sym, sym.LPAREN);
		Assert.assertEquals(lex.next_token().sym, sym.SEMI);
		Assert.assertEquals(lex.next_token().sym, sym.SEMI);
		Assert.assertEquals(lex.next_token().sym, sym.RPAREN);
		Assert.assertEquals(lex.next_token().sym, sym.EOF);
	}

}
