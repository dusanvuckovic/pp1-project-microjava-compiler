package Primer3;

public class Primer3 {

	public static void main(String[] args) throws java.io.IOException {
		Yylex lexer = new Yylex(System.in);
		//try {
			 int token = lexer.yylex();
		   while( token != sym.EOF) {
			    obradi(token);
			    token = lexer.yylex();
       }
    //} catch (java.io.IOException e) {
		//   System.err.println("Greska:" + e.getMessage());
		//}
	}	
	
	public static void obradi(int token)
	{
		switch(token)
		{
			case sym.PLUS: System.out.println("Simbol : +"); break;
			case sym.TIMES: System.out.println("Simbol : *"); break;
			case sym.NUMBER: System.out.println("Broj"); break;
			case sym.SEMI: System.out.println("Simbol : ;"); break;
			case sym.LPAREN: System.out.println("Leva"); break;
			case sym.RPAREN: System.out.println("Desna"); break;
		}
	}
}
