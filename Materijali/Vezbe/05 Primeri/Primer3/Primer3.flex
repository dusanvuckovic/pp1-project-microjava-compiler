package Primer3;
%%

//vraca integer tokene
%integer

// na kraju fajla vrati EOF
%eofval{
return sym.EOF;
%eofval}
%%
";"     { return sym.SEMI; }
"+"     { return sym.PLUS; }
"*"     { return sym.TIMES; }
"("     { return sym.LPAREN; }
")"     { return sym.RPAREN; }
[0-9]+  { return sym.NUMBER; }
[\t\n \r]	{	/* ignorise prazna mesta */			}
.			  {	System.err.println("Ilegalni karakter: "+yytext());	} 

