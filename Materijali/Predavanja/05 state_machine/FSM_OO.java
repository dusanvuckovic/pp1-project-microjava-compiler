class FSM_OO
{
  State A = new StateA();
  State B = new StateB();
  State ERROR = new StateERROR();

  interface State {
	boolean accepting();
	void handleInput(FSM_OO fsm, char ch);
  };
  
  
  class StateA implements State 
  {
 	public boolean accepting() { return true;}
	public void handleInput(FSM_OO fsm, char ch)
	{
      	if ( ch=='a' )
          fsm.setState(B);
        else 
          fsm.setState(ERROR);
	}

  };

  class StateB implements State 
  {
 	public boolean accepting() { return false;}
	public void handleInput(FSM_OO fsm, char ch)
	{
      	if ( ch=='a' )
          fsm.setState(A);
        else if ( ch=='b' )
          fsm.setState(B);
	    else 
		  fsm.setState(ERROR);
	}

  };

    class StateERROR implements State 
  {
 	public boolean accepting() { return false;}
	public  void handleInput(FSM_OO fsm, char ch)
	{
	   fsm.setState(ERROR);
	}

  };

  private State state;
    public void setState(State state) {
		this.state = state;
	}
  
    public State getState() {
		return state;
	}

	void handleInput(char ch)
	{
	   state.handleInput(this,ch);
	}

  public static void main(String[] args) throws java.io.IOException
  {
	int ch;
	FSM_OO fsm = new FSM_OO();
    fsm.setState(fsm.A);
    while ((ch = System.in.read ()) != '\n')
      fsm.handleInput((char)ch);
    
    if (fsm.getState().accepting()) 
      System.out.println("Prihvata");
    else
      System.out.println("Odbija");
  }
}
