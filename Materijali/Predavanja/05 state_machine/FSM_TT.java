
class FSM_TT
{

enum State {
    A( true ),
    B( false ),
    ERROR( false );

    final boolean accepting;

    State( boolean accepting ) {
        this.accepting = accepting;
    }
}

static State state;
static State transition[][] = 
{
	{ State.B, State.ERROR},
	{ State.A, State.B},
	{ State.ERROR, State.ERROR}
};  
  static void handleInput(char ch)
  {
    int index;
	switch( ch ) 
    {
      case 'a':
	    index=0;
		break;
	  case 'b':
		index=1;
		break;
	  default:
	    state=State.ERROR;
		return;
	}
    state = transition[state.ordinal()][index];
  }

  public static void main(String[] args) throws java.io.IOException
  {
	int ch;
    state = State.A;
    while ((ch = System.in.read ()) != '\n')
      handleInput((char)ch);
    
    if (state.accepting) 
      System.out.println("Prihvata");
    else
      System.out.println("Odbija");
  }
}
