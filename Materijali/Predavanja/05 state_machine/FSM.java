class FSM
{

enum State {
    A( true ),
    B( false ),
    ERROR( false );
    static public final Integer length = 1 + ERROR.ordinal();

    final boolean accepting;

    State( boolean accepting ) {
        this.accepting = accepting;
    }
}

static State state;
  
  static void handleInput(char ch)
  {
    switch( state ) 
    {
      case A:
      	if ( ch=='a' )
          state = State.B;
        else 
          state = State.ERROR;
        break;
      
      case B:
      	if ( ch=='a' )
          state = State.A;
        else if ( ch=='b' )
          state = State.B;
        else
          state = State.ERROR;
        break;

      case ERROR:
          //state = State.ERROR;
        break;
    }
  }
  
  public static void main(String[] args) throws java.io.IOException
  {
	int ch;
    state = State.A;
    while ((ch = System.in.read ()) != '\n')
      handleInput((char)ch);
    
    if (state.accepting) 
      System.out.println("Prihvata");
    else
      System.out.println("Odbija");
  }
}
