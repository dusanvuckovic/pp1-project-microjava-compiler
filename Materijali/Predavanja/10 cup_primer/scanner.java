// Simple Example Scanner Class

import java_cup.runtime.*;

public class scanner {
  /* single lookahead character */
  protected static int next_char;
  // since cup v11 we use SymbolFactories rather than Symbols
  private static SymbolFactory sf = new ComplexSymbolFactory();

  /* advance input by one character */
  protected static void advance()
    throws java.io.IOException
    { next_char = System.in.read(); }

  /* initialize the scanner */
  public static void init()
    throws java.io.IOException
    { advance(); }

  /* recognize and return the next complete token */
  public static Symbol next_token()
    throws java.io.IOException
    {
      for (;;)
        switch (next_char)
	  {
	    case 'a': advance(); return sf.newSymbol("A",sym.A);
	    case ',': advance(); return sf.newSymbol("COMMA",sym.COMMA);
	    case '(': advance(); return sf.newSymbol("LPAREN",sym.LPAREN);
	    case ')': advance(); return sf.newSymbol("RPAREN",sym.RPAREN);

	    case -1: return sf.newSymbol("EOF",sym.EOF);

	    default: 
	      /* in this simple scanner we just ignore everything else */
	      advance();
	    break;
	  }
    }
};

