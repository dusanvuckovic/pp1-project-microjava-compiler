
//----------------------------------------------------
// The following code was generated by CUP v0.11a beta 20060608
// Tue Dec 11 22:22:22 CET 2012
//----------------------------------------------------

package rs.ac.bg.etf.pp1;

import java_cup.runtime.*;
import org.apache.log4j.*;

/** CUP v0.11a beta 20060608 generated parser.
  * @version Tue Dec 11 22:22:22 CET 2012
  */
public class MJParser extends java_cup.runtime.lr_parser {

  /** Default constructor. */
  public MJParser() {super();}

  /** Constructor which sets the default scanner. */
  public MJParser(java_cup.runtime.Scanner s) {super(s);}

  /** Constructor which sets the default scanner. */
  public MJParser(java_cup.runtime.Scanner s, java_cup.runtime.SymbolFactory sf) {super(s,sf);}

  /** Production table. */
  protected static final short _production_table[][] = 
    unpackFromStrings(new String[] {
    "\000\046\000\002\002\010\000\002\002\004\000\002\003" +
    "\004\000\002\003\002\000\002\005\005\000\002\006\003" +
    "\000\002\004\004\000\002\004\002\000\002\007\013\000" +
    "\002\012\003\000\002\012\002\000\002\010\005\000\002" +
    "\010\003\000\002\011\004\000\002\015\004\000\002\015" +
    "\002\000\002\016\003\000\002\016\003\000\002\024\005" +
    "\000\002\024\007\000\002\025\006\000\002\025\006\000" +
    "\002\025\007\000\002\025\005\000\002\025\004\000\002" +
    "\025\007\000\002\020\005\000\002\020\003\000\002\021" +
    "\003\000\002\023\003\000\002\023\003\000\002\023\006" +
    "\000\002\013\003\000\002\013\002\000\002\014\005\000" +
    "\002\014\003\000\002\017\003\000\002\022\003" });

  /** Access to production table. */
  public short[][] production_table() {return _production_table;}

  /** Parse-action table. */
  protected static final short[][] _action_table = 
    unpackFromStrings(new String[] {
    "\000\106\000\004\004\005\001\002\000\004\002\110\001" +
    "\002\000\004\021\006\001\002\000\006\005\ufffe\021\ufffe" +
    "\001\002\000\006\005\010\021\013\001\002\000\006\006" +
    "\ufffa\021\ufffa\001\002\000\004\021\014\001\002\000\006" +
    "\005\uffff\021\uffff\001\002\000\004\021\ufffc\001\002\000" +
    "\004\007\015\001\002\000\006\005\ufffd\021\ufffd\001\002" +
    "\000\006\006\020\021\013\001\002\000\004\021\022\001" +
    "\002\000\004\002\001\001\002\000\006\006\ufffb\021\ufffb" +
    "\001\002\000\004\010\023\001\002\000\006\011\ufff7\021" +
    "\013\001\002\000\004\021\107\001\002\000\006\011\ufff8" +
    "\012\105\001\002\000\004\011\030\001\002\000\006\011" +
    "\ufff5\012\ufff5\001\002\000\006\005\ufffe\021\ufffe\001\002" +
    "\000\006\005\032\021\013\001\002\000\014\006\ufff2\013" +
    "\ufff2\014\ufff2\021\ufff2\022\ufff2\001\002\000\014\006\044" +
    "\013\041\014\043\021\034\022\037\001\002\000\026\007" +
    "\uffdd\010\uffdd\011\uffdd\012\uffdd\013\uffdd\014\uffdd\015\uffdd" +
    "\016\uffdd\021\uffdd\022\uffdd\001\002\000\004\016\100\001" +
    "\002\000\014\006\ufff0\013\ufff0\014\ufff0\021\ufff0\022\ufff0" +
    "\001\002\000\006\020\050\021\034\001\002\000\014\006" +
    "\ufff3\013\ufff3\014\ufff3\021\ufff3\022\ufff3\001\002\000\004" +
    "\010\066\001\002\000\014\006\ufff1\013\ufff1\014\ufff1\021" +
    "\ufff1\022\ufff1\001\002\000\010\007\051\020\050\021\034" +
    "\001\002\000\006\006\ufff9\021\ufff9\001\002\000\022\007" +
    "\uffe6\011\uffe6\012\uffe6\013\uffe6\014\uffe6\015\uffe6\021\uffe6" +
    "\022\uffe6\001\002\000\022\007\uffe5\011\uffe5\012\uffe5\013" +
    "\uffe5\014\uffe5\015\uffe5\021\uffe5\022\uffe5\001\002\000\006" +
    "\007\065\015\061\001\002\000\022\007\uffe4\011\uffe4\012" +
    "\uffe4\013\uffe4\014\uffe4\015\uffe4\021\uffe4\022\uffe4\001\002" +
    "\000\016\006\uffe9\013\uffe9\014\uffe9\021\uffe9\022\uffe9\023" +
    "\uffe9\001\002\000\024\007\uffe3\010\053\011\uffe3\012\uffe3" +
    "\013\uffe3\014\uffe3\015\uffe3\021\uffe3\022\uffe3\001\002\000" +
    "\010\011\uffe0\020\050\021\034\001\002\000\004\011\064" +
    "\001\002\000\010\011\uffde\012\uffde\015\061\001\002\000" +
    "\006\011\uffe1\012\057\001\002\000\006\020\050\021\034" +
    "\001\002\000\010\011\uffdf\012\uffdf\015\061\001\002\000" +
    "\006\020\uffdc\021\uffdc\001\002\000\006\020\050\021\034" +
    "\001\002\000\022\007\uffe7\011\uffe7\012\uffe7\013\uffe7\014" +
    "\uffe7\015\uffe7\021\uffe7\022\uffe7\001\002\000\022\007\uffe2" +
    "\011\uffe2\012\uffe2\013\uffe2\014\uffe2\015\uffe2\021\uffe2\022" +
    "\uffe2\001\002\000\016\006\uffea\013\uffea\014\uffea\021\uffea" +
    "\022\uffea\023\uffea\001\002\000\006\020\050\021\034\001" +
    "\002\000\006\011\070\015\061\001\002\000\004\007\071" +
    "\001\002\000\016\006\uffeb\013\uffeb\014\uffeb\021\uffeb\022" +
    "\uffeb\023\uffeb\001\002\000\014\013\041\014\043\015\061" +
    "\021\034\022\037\001\002\000\014\006\uffef\013\uffef\014" +
    "\uffef\021\uffef\022\uffef\001\002\000\016\006\ufff1\013\ufff1" +
    "\014\ufff1\021\ufff1\022\ufff1\023\075\001\002\000\012\013" +
    "\041\014\043\021\034\022\037\001\002\000\014\006\uffee" +
    "\013\uffee\014\uffee\021\uffee\022\uffee\001\002\000\016\006" +
    "\uffe8\013\uffe8\014\uffe8\021\uffe8\022\uffe8\023\uffe8\001\002" +
    "\000\010\003\101\020\050\021\034\001\002\000\004\007" +
    "\104\001\002\000\006\007\103\015\061\001\002\000\016" +
    "\006\uffed\013\uffed\014\uffed\021\uffed\022\uffed\023\uffed\001" +
    "\002\000\016\006\uffec\013\uffec\014\uffec\021\uffec\022\uffec" +
    "\023\uffec\001\002\000\004\021\013\001\002\000\006\011" +
    "\ufff6\012\ufff6\001\002\000\006\011\ufff4\012\ufff4\001\002" +
    "\000\004\002\000\001\002" });

  /** Access to parse-action table. */
  public short[][] action_table() {return _action_table;}

  /** <code>reduce_goto</code> table. */
  protected static final short[][] _reduce_table = 
    unpackFromStrings(new String[] {
    "\000\106\000\004\002\003\001\001\000\002\001\001\000" +
    "\002\001\001\000\004\003\006\001\001\000\006\005\011" +
    "\006\010\001\001\000\004\004\015\001\001\000\002\001" +
    "\001\000\002\001\001\000\002\001\001\000\002\001\001" +
    "\000\002\001\001\000\006\006\016\007\020\001\001\000" +
    "\002\001\001\000\002\001\001\000\002\001\001\000\002" +
    "\001\001\000\012\006\023\010\024\011\026\012\025\001" +
    "\001\000\002\001\001\000\002\001\001\000\002\001\001" +
    "\000\002\001\001\000\004\003\030\001\001\000\006\005" +
    "\011\006\010\001\001\000\004\015\032\001\001\000\012" +
    "\016\037\017\034\024\035\025\041\001\001\000\002\001" +
    "\001\000\002\001\001\000\002\001\001\000\012\017\051" +
    "\020\071\021\044\023\045\001\001\000\002\001\001\000" +
    "\002\001\001\000\002\001\001\000\012\017\051\020\046" +
    "\021\044\023\045\001\001\000\002\001\001\000\002\001" +
    "\001\000\002\001\001\000\004\022\061\001\001\000\002" +
    "\001\001\000\002\001\001\000\002\001\001\000\016\013" +
    "\053\014\055\017\051\020\054\021\044\023\045\001\001" +
    "\000\002\001\001\000\004\022\061\001\001\000\002\001" +
    "\001\000\012\017\051\020\057\021\044\023\045\001\001" +
    "\000\004\022\061\001\001\000\002\001\001\000\010\017" +
    "\051\021\062\023\045\001\001\000\002\001\001\000\002" +
    "\001\001\000\002\001\001\000\012\017\051\020\066\021" +
    "\044\023\045\001\001\000\004\022\061\001\001\000\002" +
    "\001\001\000\002\001\001\000\014\016\072\017\034\022" +
    "\061\024\035\025\073\001\001\000\002\001\001\000\002" +
    "\001\001\000\010\017\034\024\075\025\076\001\001\000" +
    "\002\001\001\000\002\001\001\000\012\017\051\020\101" +
    "\021\044\023\045\001\001\000\002\001\001\000\004\022" +
    "\061\001\001\000\002\001\001\000\002\001\001\000\006" +
    "\006\023\011\105\001\001\000\002\001\001\000\002\001" +
    "\001\000\002\001\001" });

  /** Access to <code>reduce_goto</code> table. */
  public short[][] reduce_table() {return _reduce_table;}

  /** Instance of action encapsulation class. */
  protected CUP$MJParser$actions action_obj;

  /** Action encapsulation object initializer. */
  protected void init_actions()
    {
      action_obj = new CUP$MJParser$actions(this);
    }

  /** Invoke a user supplied parse action. */
  public java_cup.runtime.Symbol do_action(
    int                        act_num,
    java_cup.runtime.lr_parser parser,
    java.util.Stack            stack,
    int                        top)
    throws java.lang.Exception
  {
    /* call code in generated class */
    return action_obj.CUP$MJParser$do_action(act_num, parser, stack, top);
  }

  /** Indicates start state. */
  public int start_state() {return 0;}
  /** Indicates start production. */
  public int start_production() {return 1;}

  /** <code>EOF</code> Symbol index. */
  public int EOF_sym() {return 0;}

  /** <code>error</code> Symbol index. */
  public int error_sym() {return 1;}


  /** Scan to get the next Symbol. */
  public java_cup.runtime.Symbol scan()
    throws java.lang.Exception
    {

	Symbol s = this.getScanner().next_token();
	if (s != null && s.value != null) 
		log.info(s.toString() + " " + s.value.toString());
	return s;

    }


	
	int printCallCount = 0;
	
	Logger log = Logger.getLogger(getClass());
   
   
    // slede redefinisani metodi za prijavu gresaka radi izmene teksta poruke
     
    public void report_fatal_error(String message, Object info) throws java.lang.Exception {
      done_parsing();
      report_error(message, info);
    }
  
    public void syntax_error(Symbol cur_token) {
        report_error("\nSintaksna greska", cur_token);
    }
  
    public void unrecovered_syntax_error(Symbol cur_token) throws java.lang.Exception {
        report_fatal_error("Fatalna greska, parsiranje se ne moze nastaviti", cur_token);
    }

    public void report_error(String message, Object info) {
    	StringBuilder msg = new StringBuilder(message); 
    	if (info instanceof Symbol)
            msg.append (" na liniji ").append(((Symbol)info).left);
        log.error(msg.toString());
    }

}

/** Cup generated class to encapsulate user supplied action code.*/
class CUP$MJParser$actions {
  private final MJParser parser;

  /** Constructor */
  CUP$MJParser$actions(MJParser parser) {
    this.parser = parser;
  }

  /** Method with the actual generated action code. */
  public final java_cup.runtime.Symbol CUP$MJParser$do_action(
    int                        CUP$MJParser$act_num,
    java_cup.runtime.lr_parser CUP$MJParser$parser,
    java.util.Stack            CUP$MJParser$stack,
    int                        CUP$MJParser$top)
    throws java.lang.Exception
    {
      /* Symbol object for return from actions */
      java_cup.runtime.Symbol CUP$MJParser$result;

      /* select the action based on the action number */
      switch (CUP$MJParser$act_num)
        {
          /*. . . . . . . . . . . . . . . . . . . .*/
          case 37: // Addop ::= PLUS 
            {
              Object RESULT =null;

              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("Addop",16, ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 36: // Designator ::= IDENT 
            {
              Object RESULT =null;

              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("Designator",13, ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 35: // ActualParamList ::= Expr 
            {
              Object RESULT =null;

              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("ActualParamList",10, ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 34: // ActualParamList ::= ActualParamList COMMA Expr 
            {
              Object RESULT =null;

              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("ActualParamList",10, ((java_cup.runtime.Symbol)CUP$MJParser$stack.elementAt(CUP$MJParser$top-2)), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 33: // ActualPars ::= 
            {
              Object RESULT =null;

              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("ActualPars",9, ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 32: // ActualPars ::= ActualParamList 
            {
              Object RESULT =null;

              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("ActualPars",9, ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 31: // Factor ::= Designator LPAREN ActualPars RPAREN 
            {
              Object RESULT =null;

              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("Factor",17, ((java_cup.runtime.Symbol)CUP$MJParser$stack.elementAt(CUP$MJParser$top-3)), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 30: // Factor ::= Designator 
            {
              Object RESULT =null;

              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("Factor",17, ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 29: // Factor ::= NUMBER 
            {
              Object RESULT =null;

              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("Factor",17, ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 28: // Term ::= Factor 
            {
              Object RESULT =null;

              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("Term",15, ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 27: // Expr ::= Term 
            {
              Object RESULT =null;

              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("Expr",14, ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 26: // Expr ::= Expr Addop Term 
            {
              Object RESULT =null;

              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("Expr",14, ((java_cup.runtime.Symbol)CUP$MJParser$stack.elementAt(CUP$MJParser$top-2)), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 25: // Matched ::= IF Expr Matched ELSE Matched 
            {
              Object RESULT =null;

              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("Matched",19, ((java_cup.runtime.Symbol)CUP$MJParser$stack.elementAt(CUP$MJParser$top-4)), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 24: // Matched ::= RETURN SEMI 
            {
              Object RESULT =null;

              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("Matched",19, ((java_cup.runtime.Symbol)CUP$MJParser$stack.elementAt(CUP$MJParser$top-1)), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 23: // Matched ::= RETURN Expr SEMI 
            {
              Object RESULT =null;

              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("Matched",19, ((java_cup.runtime.Symbol)CUP$MJParser$stack.elementAt(CUP$MJParser$top-2)), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 22: // Matched ::= PRINT LPAREN Expr RPAREN SEMI 
            {
              Object RESULT =null;
		 parser.printCallCount++; 
              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("Matched",19, ((java_cup.runtime.Symbol)CUP$MJParser$stack.elementAt(CUP$MJParser$top-4)), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 21: // Matched ::= Designator EQUAL error SEMI 
            {
              Object RESULT =null;
		 parser.log.debug("Uspesan oporavak od greske pri dodeli vrednosti."); 
              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("Matched",19, ((java_cup.runtime.Symbol)CUP$MJParser$stack.elementAt(CUP$MJParser$top-3)), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 20: // Matched ::= Designator EQUAL Expr SEMI 
            {
              Object RESULT =null;

              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("Matched",19, ((java_cup.runtime.Symbol)CUP$MJParser$stack.elementAt(CUP$MJParser$top-3)), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 19: // Unmatched ::= IF Expr Matched ELSE Unmatched 
            {
              Object RESULT =null;

              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("Unmatched",18, ((java_cup.runtime.Symbol)CUP$MJParser$stack.elementAt(CUP$MJParser$top-4)), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 18: // Unmatched ::= IF Expr Statement 
            {
              Object RESULT =null;

              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("Unmatched",18, ((java_cup.runtime.Symbol)CUP$MJParser$stack.elementAt(CUP$MJParser$top-2)), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 17: // Statement ::= Unmatched 
            {
              Object RESULT =null;

              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("Statement",12, ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 16: // Statement ::= Matched 
            {
              Object RESULT =null;

              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("Statement",12, ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 15: // StatementList ::= 
            {
              Object RESULT =null;

              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("StatementList",11, ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 14: // StatementList ::= StatementList Statement 
            {
              Object RESULT =null;

              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("StatementList",11, ((java_cup.runtime.Symbol)CUP$MJParser$stack.elementAt(CUP$MJParser$top-1)), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 13: // FormalParamDecl ::= Type IDENT 
            {
              Object RESULT =null;

              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("FormalParamDecl",7, ((java_cup.runtime.Symbol)CUP$MJParser$stack.elementAt(CUP$MJParser$top-1)), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 12: // FormalParamList ::= FormalParamDecl 
            {
              Object RESULT =null;

              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("FormalParamList",6, ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 11: // FormalParamList ::= FormalParamList COMMA FormalParamDecl 
            {
              Object RESULT =null;

              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("FormalParamList",6, ((java_cup.runtime.Symbol)CUP$MJParser$stack.elementAt(CUP$MJParser$top-2)), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 10: // FormPars ::= 
            {
              Object RESULT =null;

              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("FormPars",8, ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 9: // FormPars ::= FormalParamList 
            {
              Object RESULT =null;

              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("FormPars",8, ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 8: // MethodDecl ::= Type IDENT LPAREN FormPars RPAREN VarDeclList LBRACE StatementList RBRACE 
            {
              Object RESULT =null;

              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("MethodDecl",5, ((java_cup.runtime.Symbol)CUP$MJParser$stack.elementAt(CUP$MJParser$top-8)), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 7: // MethodDeclList ::= 
            {
              Object RESULT =null;

              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("MethodDeclList",2, ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 6: // MethodDeclList ::= MethodDeclList MethodDecl 
            {
              Object RESULT =null;

              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("MethodDeclList",2, ((java_cup.runtime.Symbol)CUP$MJParser$stack.elementAt(CUP$MJParser$top-1)), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 5: // Type ::= IDENT 
            {
              Object RESULT =null;

              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("Type",4, ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 4: // VarDecl ::= Type IDENT SEMI 
            {
              Object RESULT =null;

              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("VarDecl",3, ((java_cup.runtime.Symbol)CUP$MJParser$stack.elementAt(CUP$MJParser$top-2)), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 3: // VarDeclList ::= 
            {
              Object RESULT =null;

              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("VarDeclList",1, ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 2: // VarDeclList ::= VarDeclList VarDecl 
            {
              Object RESULT =null;

              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("VarDeclList",1, ((java_cup.runtime.Symbol)CUP$MJParser$stack.elementAt(CUP$MJParser$top-1)), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 1: // $START ::= Program EOF 
            {
              Object RESULT =null;
		int start_valleft = ((java_cup.runtime.Symbol)CUP$MJParser$stack.elementAt(CUP$MJParser$top-1)).left;
		int start_valright = ((java_cup.runtime.Symbol)CUP$MJParser$stack.elementAt(CUP$MJParser$top-1)).right;
		Object start_val = (Object)((java_cup.runtime.Symbol) CUP$MJParser$stack.elementAt(CUP$MJParser$top-1)).value;
		RESULT = start_val;
              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("$START",0, ((java_cup.runtime.Symbol)CUP$MJParser$stack.elementAt(CUP$MJParser$top-1)), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          /* ACCEPT */
          CUP$MJParser$parser.done_parsing();
          return CUP$MJParser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 0: // Program ::= PROG IDENT VarDeclList LBRACE MethodDeclList RBRACE 
            {
              Object RESULT =null;

              CUP$MJParser$result = parser.getSymbolFactory().newSymbol("Program",0, ((java_cup.runtime.Symbol)CUP$MJParser$stack.elementAt(CUP$MJParser$top-5)), ((java_cup.runtime.Symbol)CUP$MJParser$stack.peek()), RESULT);
            }
          return CUP$MJParser$result;

          /* . . . . . .*/
          default:
            throw new Exception(
               "Invalid action number found in internal parse table");

        }
    }
}

