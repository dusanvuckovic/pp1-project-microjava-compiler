package rs.ac.bg.etf.pp1;

public class sym_old {
	
	//keywords
	public static final int PROG = 1;
	public static final int PRINT = 2;
	public static final int RETURN = 3;
	//keywords
	
	//identifiers
	public static final int IDENT = 4; 
	//identifiers
	
	//constants
	public static final int NUMBER = 5;
	//constants
	
	//operator
	public static final int PLUS = 6;
	public static final int EQUAL = 7;
	public static final int COMMA = 8;
	public static final int SEMI = 9;
	public static final int LPAREN = 10;
	public static final int RPAREN = 11;
	public static final int LBRACE = 12;
	public static final int RBRACE = 13;	
	//operator
	
	public static final int EOF = 14;
	public static final int VOID = 15;

};
 
/*
 * Ključne reči: program, print, return
Identifikatori: ident = letter {letter | digit | "_"}.
Celobrojne konstante: number = digit {digit}.
Operatori: + = , ; ( ) { }
Komentari: // ... do kraja linije
Tipovi: int
 * 
 * */
