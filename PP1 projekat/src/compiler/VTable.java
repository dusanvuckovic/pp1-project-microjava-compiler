package compiler;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import compiler.tab.TabRedux;
import rs.etf.pp1.mj.runtime.Code;
import rs.etf.pp1.symboltable.concepts.Obj;
import rs.etf.pp1.symboltable.concepts.Scope;
import rs.etf.pp1.symboltable.concepts.Struct;

public final class VTable {
	
	private List<Byte> vtable = new ArrayList();
	
	private final byte CONST_CODE = (byte) Code.const_;
	private final byte PUTSTATIC_CODE = (byte) Code.putstatic;
	
	private void addWord(int word) {
		byte byte1 = (byte) (word >> 24),
			 byte2 = (byte) (word >> 16),
			 byte3 = (byte) (word >> 8),
			 byte4 = (byte) word;
		
		vtable.add(byte1);
		vtable.add(byte2);
		vtable.add(byte3);
		vtable.add(byte4);		
	}
	
	private void addHalfWord(int halfword) {
		byte byte1 = (byte) (halfword >> 8),
			 byte2 = (byte) halfword;
		
		vtable.add(byte1);
		vtable.add(byte2);			
	}
	
	private void addWordToStaticData(int value, int address) {
		vtable.add(CONST_CODE);
		addWord(value);
		vtable.add(PUTSTATIC_CODE);
		addHalfWord(address);					
	}
	
	private void addNameTerminator() {
		addWordToStaticData(-1, Code.dataSize++);				
	}
	private void addFunctionAddress(int functionAddress) {
		addWordToStaticData(functionAddress, Code.dataSize++);			
	}	
	
	private void addFunctionEntry(String name, int functionAddress) {
		for (int i = 0; i < name.length(); i++) {
			char letter = name.charAt(i);
			addWordToStaticData(letter, Code.dataSize++);			
		}		
		addNameTerminator();
		addFunctionAddress(functionAddress);
	}
	
	private void addTableTerminator() {
		addWordToStaticData(-2, Code.dataSize++);				
	}
	
	public void generateTableAndSetPC(Scope programScope) {
		Collection<Obj> collection = programScope.getLocals().symbols();		
		for (Obj classTypeObj: collection) {
			if (classTypeObj.getKind() != Obj.Type || classTypeObj.getType().getKind() != Struct.Class)
				continue;
			Obj vt = TabRedux.findInClass(classTypeObj, "@vtable");
			vt.setAdr(Code.dataSize);			
			for (Obj classMember: classTypeObj.getType().getMembers()) {									
				if (classMember.getKind() != Obj.Meth)
					continue;
				addFunctionEntry(classMember.getName(), classMember.getAdr());						
			}
			addTableTerminator();
		}		
		writeOutAndSetPC();
	}
	
	public boolean checkIfTableExists(Scope programScope) {
		Collection<Obj> collection = programScope.getLocals().symbols();		
		for (Obj classTypeObj: collection) {
			if (classTypeObj.getKind() != Obj.Type || classTypeObj.getType().getKind() != Struct.Class) {
				continue;				
			}
			return true;
		}		
		return false;		
	}
	
	public void writeOutAndSetPC() {
		Code.mainPc = Code.pc;
		for (byte b: vtable) {
			Code.buf[Code.pc++] = b;			
		}						
	}

}
