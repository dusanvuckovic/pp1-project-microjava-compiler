package compiler;

public final class ElementCounter {
	
	private int globalVariables = 0,
				localVariables = 0,
				globalConstants = 0,
				globalAndStaticInnerClassFunctions = 0,
				functionCallsInMain = 0,
				formalParameterDeclarations = 0,
				innerClassDeclarations = 0,
				innerClassMethodDeclarations = 0,
				innerClassFieldDeclaration = 0;
		
	public void foundGlobalVariable() {
		++globalVariables;
	}
	
	public void foundLocalVariable() {
		++localVariables;
	}
	
	public void foundGlobalConstant() {
		++globalConstants;
	}
	
	public void foundGlobalStaticFunction() {
		++globalAndStaticInnerClassFunctions;
	}
	
	public void foundFunctionCallInMain() {
		++functionCallsInMain;
	}
	
	public void foundFormalParameter() {
		++formalParameterDeclarations;
	}
	
	public void foundInnerClassDeclaration() {
		++innerClassDeclarations;
	}
	
	public void foundInnerClassMethodDeclaration() {
		++innerClassMethodDeclarations;
	}

	public void foundInnerClassFieldDeclaration() {
		++innerClassFieldDeclaration;
	}
		

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(System.lineSeparator());
		sb.append("Globalne promenljive: " + globalVariables + System.lineSeparator());
		sb.append("Lokalne promenljive: " + localVariables + System.lineSeparator());
		sb.append("Globalne konstante: " + globalConstants + System.lineSeparator());
		sb.append("Globalne i stat.un. f-je: " + globalAndStaticInnerClassFunctions + System.lineSeparator());
		sb.append("Pozivi f-ja u main: " + functionCallsInMain + System.lineSeparator());
		sb.append("Deklaracije formalnih argumenata: " + formalParameterDeclarations + System.lineSeparator());
		sb.append("Unutrašnje klase: " + innerClassDeclarations + System.lineSeparator());
		sb.append("Metodi unutrašnjih klasa: " + innerClassMethodDeclarations + System.lineSeparator());
		sb.append("Polja unutrašnjih klasa: " + innerClassFieldDeclaration + System.lineSeparator());
		return sb.toString();		
	}
	
}
