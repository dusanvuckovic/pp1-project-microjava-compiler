package compiler.tab;

import java.util.HashMap;
import java.util.Map;

import rs.etf.pp1.mj.runtime.Code;
import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.Obj;
import rs.etf.pp1.symboltable.concepts.Scope;
import rs.etf.pp1.symboltable.concepts.Struct;

public class TabRedux extends Tab {
	
	private static Map<Struct, String> structNames = new HashMap();
	
	public static final Struct boolType = new Struct(Struct.Bool);			
	
	static {
		structNames.put(intType, "int");
		structNames.put(charType, "char");
		structNames.put(boolType, "bool");
	}
	
	public static final void addType(Struct type, String name) {
		structNames.put(type, name);		
	}
	public static final String getTypeName(Struct type) {
		return structNames.get(type);		
	}	
	
	public static void initR() {		
		TabRedux.init();
		Scope tempScope = currentScope();
		while (tempScope.getOuter() != null)
			tempScope = tempScope.getOuter();
		tempScope.addToLocals(new Obj(Obj.Type, "bool", boolType));		
	}
		
	//ako je deklarisana kao konstanta
	public static boolean isGlobalConstant(Obj o) {
		return o.getKind() == Obj.Con;		
	}
	
	//ako je nivo 1 (main) i ako je promenljiva
	public static boolean isLocalVariable(Obj o) {
		return (o.getKind() == Obj.Var) && (o.getLevel() == 1);				
	}
	
	//ako je nivo 0 (program) i ako je promenljiva
	public static boolean isGlobalVariable(Obj o) {
		return (o.getKind() == Obj.Var) && (o.getLevel() == 0);				
	}
	
	public static boolean isArray(Obj o) {
		return (o.getType().getKind() == Struct.Array);				
	}
	
	public static boolean isClassOrArray(Obj o) {
		return (o.getType().getKind() == Struct.Array || o.getType().getKind() == Struct.Class);				
	}
	
	public static boolean isVariableArrayOrField(Obj o) {
		return o.getKind() == Obj.Var || o.getKind() == Obj.Elem || o.getKind() == Obj.Fld;		
	}
	
	public static Obj findInClass(Obj classObj, String name) {
		Obj res = TabRedux.noObj;
		for (Obj sym: classObj.getType().getMembers()) {
			if (sym.getName().equals(name))
				res = sym;			
		}
		return res;
	}
	
	public static Obj findInClass(Obj classObj, Obj name) {
		return findInClass(classObj, name.getName());
	}

}
