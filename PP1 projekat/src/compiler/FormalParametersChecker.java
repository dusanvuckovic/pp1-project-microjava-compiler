package compiler;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import compiler.tab.TabRedux;
import rs.etf.pp1.symboltable.concepts.Obj;
import rs.etf.pp1.symboltable.concepts.Struct;

public class FormalParametersChecker {
			
	private static String
		ERROR_TOO_FEW   = "premalo parametara pri pozivu f-je!",
		ERROR_WRONG_NUM = "loš broj parametara pri pozivu f-je!",
		ERROR_BAD_MATCH_1 = "nepoklapanje: " + System.lineSeparator() + "formalni parametri su: ",
		ERROR_BAD_MATCH_2 = System.lineSeparator() + "aktuelni parametri su: ";					
	
	private String foundError = "";
	private List<Struct> formPars;
	private Struct varArg = null;		
	private Obj varArgObj = null;
		
	public boolean hasVarArgs() {
		return varArg != null;		
	}
	
	public FormalParametersChecker(List<Struct> formPars) {
		this.formPars = formPars;
	}
	
	public FormalParametersChecker(List<Struct> formPars, Struct varArg) {
		this(formPars);
		this.varArg = varArg;
	}
	
	public FormalParametersChecker() {
		formPars = new LinkedList<Struct>();
	}
	
	public void addFormalParameter(Struct s) {
		formPars.add(s);	
	}
	
	public void addVarArg(Obj s) {
		varArgObj = s;
		varArg = varArgObj.getType().getElemType();
	}
	
	private String neededParameters(List<Struct> list) {
		if (list.isEmpty())
			return "broj parametara je nula!";
		String s = "";
		for (int i = 0; i < list.size(); i++) {		
			if (i != 0)
				s += ", ";
			s += TabRedux.getTypeName(list.get(i));						
		}				
		if (hasVarArgs())
			s += ", " + TabRedux.getTypeName(varArg) + "...";
		return s;
	}
	
	public String getError() {
		return foundError;	
	}
		
	public boolean checkParameters(List<Struct> toCheck) {
		foundError = "";
		
		if (hasVarArgs() && toCheck.size() <= formPars.size()) {
			foundError = ERROR_TOO_FEW;
			return false;
		}
		if (!hasVarArgs() && toCheck.size() != formPars.size()) {
			foundError = ERROR_WRONG_NUM;
			return false;
		}
		
		Iterator<Struct> check = toCheck.iterator(), form = formPars.iterator();
		while (form.hasNext()) {			
			Struct fp = form.next(),
				   ap = check.next();
			if (fp.compatibleWith(ap))
				continue;			
			foundError = ERROR_BAD_MATCH_1 + neededParameters(formPars) + ERROR_BAD_MATCH_2 + neededParameters(toCheck);
			return false;
		}
						
		if (!hasVarArgs())
			return true;
				
		while (check.hasNext()) {
			if (!check.next().equals(varArg)) {
				foundError = ERROR_BAD_MATCH_1 + neededParameters(formPars) + ERROR_BAD_MATCH_2 + neededParameters(toCheck);
				return false;			
			}			
		}
		return true;
	}
	
	public Struct getVarArg() {
		return varArg;		
	}
	
	public Obj getVarArgObj() {
		return varArgObj;		
	}
	
	public int formalParameterNumber() {
		return formPars.size();		
	}
	
	public boolean checkParameters(ActualParametersChecker apc) {
		return checkParameters(apc.getTypesInOrder());
	}

}
