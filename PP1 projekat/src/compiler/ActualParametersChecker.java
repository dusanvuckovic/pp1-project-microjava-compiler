package compiler;

import java.util.LinkedList;
import java.util.List;

import rs.etf.pp1.symboltable.concepts.Obj;
import rs.etf.pp1.symboltable.concepts.Struct;

public class ActualParametersChecker {
	
	private List<Obj> actPars;
	
	public ActualParametersChecker() {
		actPars = new LinkedList();		
	}
	
	public void addActualParameter(Obj o) {
		actPars.add(o);
	}
	
	public List<Struct> getTypesInOrder() {
		List<Struct> typeList = new LinkedList();
		for (Obj o: actPars)
			typeList.add(o.getType());
		return typeList;		
	}
	
	/*videti: vraća li ovo uvek integer
	i kako raditi sa nizovima klasa
	tj. varargs-ovima klasa	 */	
	public List<Integer> getActualValues() {
		List<Integer> valueList = new LinkedList();
		for (Obj o: actPars)
			valueList.add(o.getAdr());
		return valueList;						
	}
	
	public boolean check(FormalParametersChecker fc) {
		return fc.checkParameters(getTypesInOrder());		
	}
	
	public List<Integer> getArguments(FormalParametersChecker fc) {
		List<Integer> values = getActualValues();
		values = values.subList(fc.formalParameterNumber(), values.size());
		return values;						
	}
	
	public void clear() {
		actPars.clear();
	}


}
