package compiler;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import rs.etf.pp1.symboltable.concepts.Obj;
import rs.etf.pp1.symboltable.concepts.Struct;

public class VirtualMethodChecker {
	
	private Map<Struct, List<String>> virtualMethodsOfClasses = new HashMap();
	
	public void addVirtualMethodOfClass(Struct classType, Obj method) {
		if (!virtualMethodsOfClasses.containsKey(classType))
			virtualMethodsOfClasses.put(classType, new LinkedList<String>());
		virtualMethodsOfClasses.get(classType).add(method.getName());
	}
	
	public boolean isClassMethodVirtual(Struct classType, Obj method) {
		return isClassMethodVirtual(classType, method.getName());				
	}
	
	public boolean isClassMethodVirtual(Struct classType, String methodName) {
		if (!virtualMethodsOfClasses.containsKey(classType))
			return false;
		return virtualMethodsOfClasses.get(classType).contains(methodName);
	}

}
