package compiler.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.logging.log4j.Logger;

import compiler.Lexer;
import compiler.Parser;
import compiler.tab.DumpSymbolTableVisitorRedux;
import compiler.tab.TabRedux;
import config.Log4JUtils;
import rs.etf.pp1.mj.runtime.Code;

public final class ParserTest {

	private static Lexer lexer;
	private static Parser parser;
	private static Logger logger;
	private static Path inputPath;
	private static Path outputPath;
		
	private static String OUTPUT_LOCATION = "test/program.obj";
	
	public static void main(String[] args) {
		if (args.length == 0)
			System.out.println("Morate navesti put do programskog fajla!");		
		inputPath = Paths.get(args[0]);				
		outputPath = Paths.get(OUTPUT_LOCATION);
		System.out.println(inputPath.toAbsolutePath());
		try (BufferedReader br = Files.newBufferedReader(inputPath)) {			
			lexer = new Lexer(br);
			parser = new Parser(lexer);						
			logger = Log4JUtils.getLogger();
			logger.info("Logging file: " + inputPath.toAbsolutePath());
			TabRedux.initR();		
			testPars(lexer, parser, logger, outputPath);		
		} catch (IOException e) {
			logger.error("Nije pronađen put do fajla za test!");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Greška pri parsiranju!");
		}
		
	}
	
	private static void testPars(Lexer lexer, Parser parser, Logger logger, Path output) throws Exception {		
		parser.parse();
		dump();
		if (!parser.errorsWhileParsing()) {
			Files.deleteIfExists(Paths.get(("test/program.obj")));
			Code.write(Files.newOutputStream(output));
			logger.info(parser.ec);		
			logger.info("Uspešno parsiranje!");
		}
		else
			logger.error("Neuspešno parsiranje!");
	}	
	
	static void dump() {
		TabRedux.dump(new DumpSymbolTableVisitorRedux());		
	}

}
