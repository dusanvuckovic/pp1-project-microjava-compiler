package compiler.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import compiler.Lexer;
import compiler.Symbols;
import java_cup.runtime.Symbol;

public final class LexerTest {
	
	
	private static Lexer lexer;		
	private static Path inputPath;

	public static void main(String[] args) throws FileNotFoundException {
		
		if (args.length == 0 || args.length > 2) {
			System.out.println("Loš ulaz sa komandne linije!");
			return;
		}
		inputPath = Paths.get(args[0]);
		if (args.length == 2) 
			System.setOut(new PrintStream(new File(args[1])));
		System.out.println(inputPath.toAbsolutePath());
		try (BufferedReader br = Files.newBufferedReader(inputPath)) {			
			lexer = new Lexer(br);											
			System.out.println("Lexer test: " + inputPath.toAbsolutePath());
									
			Symbol current_token = null;
			while ((current_token = lexer.next_token()).sym != Symbols.EOF) {
				String s = "";
				if (current_token != null)
					s += current_token;
				if (current_token.value != null)
					s += (" " + current_token.value);			
				System.out.println(s);						
			}			
						
		} catch (IOException e) {
			System.out.println("Nije pronađen put do fajla za test!");
		} catch (Exception e) {
			System.out.println("Greška pri parsiranju!");
		}			
								
	}
}

