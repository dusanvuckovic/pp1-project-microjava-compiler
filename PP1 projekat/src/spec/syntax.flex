package compiler;
import java_cup.runtime.Symbol;

%%
%{

	// ukljucivanje informacije o poziciji tokena
	private Symbol symbol(int type) {
		return new Symbol(type, yyline+1, yycolumn, yytext());
	}
	
	// ukljucivanje informacije o poziciji tokena
	private Symbol symbol(int type, Object value) {
		return new Symbol(type, yyline+1, yycolumn, value);
	}
%}

%line
%column
%class Lexer
%cupsym Symbols
%cup
%public

%xstate COMMENT
%xstate C_COMMENT
%eofval{
	return symbol(Symbols.EOF);
%eofval}


%%

" " {}
"\b" {}
"\t" {}
"\r\n" {}
"\f" {}

"program" {return symbol(Symbols.PROGRAM);}
"break" {return symbol(Symbols.BREAK);}
"class" {return symbol(Symbols.CLASS);}
"else" {return symbol(Symbols.ELSE);}
"const" {return symbol(Symbols.CONST);}
"if" {return symbol(Symbols.IF);}
"new" {return symbol(Symbols.NEW);}
"print" {return symbol(Symbols.PRINT);}
"read" {return symbol(Symbols.READ);}
"return" {return symbol(Symbols.RETURN);}
"void" {return symbol(Symbols.VOID);}
"for" {return symbol(Symbols.FOR);}
"extends" {return symbol(Symbols.EXTENDS);}
"continue" {return symbol(Symbols.CONTINUE);}
"static" {return symbol(Symbols.STATIC);}

"+"   {return symbol(Symbols.PLUS);}
"-"   {return symbol(Symbols.MINUS);}
"*"   {return symbol(Symbols.TIMES);}
"/"   {return symbol(Symbols.DIVIDE);}
"%"   {return symbol(Symbols.PERCENT);}
"=="  {return symbol(Symbols.EQUALEQUAL);}
"!="  {return symbol(Symbols.INEQUAL);}
">"   {return symbol(Symbols.GREATER);}
">="  {return symbol(Symbols.GREATEROREQUAL);}
"<"   {return symbol(Symbols.LESSER);}
"<="  {return symbol(Symbols.LESSEROREQUAL);}
"&&"  {return symbol(Symbols.ANDAND);}
"||"  {return symbol(Symbols.OROR);}
"="   {return symbol(Symbols.EQUAL);}
"+="  {return symbol(Symbols.PLUSEQUAL);}
"-="  {return symbol(Symbols.MINUSEQUAL);}
"*="  {return symbol(Symbols.TIMESEQUAL);}
"/="  {return symbol(Symbols.DIVIDEEQUAL);}
"%="  {return symbol(Symbols.PERCENTEQUAL);}
"++"  {return symbol(Symbols.PLUSPLUS);}
"--"  {return symbol(Symbols.MINUSMINUS);}
";"   {return symbol(Symbols.SEMICOLON);}
","   {return symbol(Symbols.COMMA);}
"("   {return symbol(Symbols.LPAREN);}
")"   {return symbol(Symbols.RPAREN);}
"{"   {return symbol(Symbols.LBRACE);}
"}"   {return symbol(Symbols.RBRACE);}
"["   {return symbol(Symbols.LBRACKET);}
"]"   {return symbol(Symbols.RBRACKET);}
"..." {return symbol(Symbols.ELLIPSIS);}
"."   {return symbol(Symbols.PERIOD);}

"//" 		     { yybegin(COMMENT); }
<COMMENT> .      { yybegin(COMMENT); }
<COMMENT> "\r\n" { yybegin(YYINITIAL); }

"/*"            { yybegin(C_COMMENT); }
<C_COMMENT>"*/" { yybegin(YYINITIAL); }
<C_COMMENT>\n   { }
<C_COMMENT>.    { }



[0-9]+ {return symbol(Symbols.NUMCONST, new Integer(yytext()));}
"true" {return symbol(Symbols.BOOLCONST, true);}
"false" {return symbol(Symbols.BOOLCONST, false);}
([a-z]|[A-Z])[a-z|A-Z|0-9|_]* {return symbol(Symbols.IDENT, yytext());}
"'"[\040-\176]"'" {return symbol(Symbols.CHARCONST, new Character(yytext().charAt(1)));}

. { System.out.println("Leksička greška (" + yytext() + ") na liniji " + (yyline+1) + " i koloni " + (yycolumn+1) + "!"); }
