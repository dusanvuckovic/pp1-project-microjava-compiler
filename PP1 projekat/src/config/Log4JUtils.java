package config;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.appender.ConsoleAppender;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.builder.api.AppenderComponentBuilder;
import org.apache.logging.log4j.core.config.builder.api.ComponentBuilder;
import org.apache.logging.log4j.core.config.builder.api.ConfigurationBuilder;
import org.apache.logging.log4j.core.config.builder.api.ConfigurationBuilderFactory;
import org.apache.logging.log4j.core.config.builder.api.LayoutComponentBuilder;
import org.apache.logging.log4j.core.config.builder.impl.BuiltConfiguration;

public class Log4JUtils {
	
	private static Logger logger = null;

	public static Logger getLogger() {
		if (logger == null) {
			initialize();					
		}
		return logger;		
	}	
		
	private static AppenderComponentBuilder makeConsoleAppender(ConfigurationBuilder<BuiltConfiguration> builder) {
		AppenderComponentBuilder consoleAppender = builder.newAppender("Stdout", "CONSOLE")
				.addAttribute("target",	ConsoleAppender.Target.SYSTEM_OUT);
		LayoutComponentBuilder consoleLayout = builder.newLayout("PatternLayout").
				addAttribute("pattern", "%-5level %d{yy-MM-dd HH:mm:ss} - %msg%n");
		consoleAppender.add(consoleLayout);		
		return consoleAppender;		
	}
	
	private static AppenderComponentBuilder makeConsoleErrorAppender(ConfigurationBuilder<BuiltConfiguration> builder) {
		AppenderComponentBuilder consoleAppender = builder.newAppender("Stderr", "CONSOLE")
				.addAttribute("target",	ConsoleAppender.Target.SYSTEM_ERR);
		LayoutComponentBuilder consoleLayout = builder.newLayout("PatternLayout").
				addAttribute("pattern", "%-5level %d{yy-MM-dd HH:mm:ss} - %msg%n");
		consoleAppender.add(consoleLayout);		
		return consoleAppender;		
	}
	
	private static AppenderComponentBuilder makeRollingAppender(ConfigurationBuilder<BuiltConfiguration> builder) {
		AppenderComponentBuilder rollingAppender = builder.newAppender("rolling", "RollingFile")
				.addAttribute("fileName", "logs/output.log")
				.addAttribute("filePattern", "logs/output %d{MM-dd} %d{HH}h%d{mm}m%d{ss}s.log");
		LayoutComponentBuilder layoutBuilder = builder.newLayout("PatternLayout").
				addAttribute("pattern", "%d [%t] %-5level: %msg %n");
		
		ComponentBuilder<?> triggeringPolicy = builder.newComponent("Policies")
				.addComponent(builder.newComponent("OnStartupTriggeringPolicy").
				addAttribute("minSize", 1));
		rollingAppender.add(layoutBuilder).addComponent(triggeringPolicy);
		return rollingAppender;
	}

	private static void initialize() {
		ConfigurationBuilder<BuiltConfiguration> builder = ConfigurationBuilderFactory.newConfigurationBuilder().
				setStatusLevel(Level.INFO).
				setConfigurationName("BuilderTest");					
		
		builder.add(makeConsoleAppender(builder));
		builder.add(makeRollingAppender(builder));
		
		builder.add(builder.newRootLogger(Level.INFO).
				add(builder.newAppenderRef("rolling")).
				add(builder.newAppenderRef("Stdout")));

		Configurator.initialize(builder.build());
		logger = LogManager.getRootLogger();
	}

	/*
	 * public void prepareLogFile(Logger root) { Appender appender =
	 * root.getAppender("file");
	 * 
	 * if (!(appender instanceof FileAppender)) return; FileAppender fAppender =
	 * (FileAppender)appender;
	 * 
	 * String logFileName = fAppender.getFile(); logFileName =
	 * logFileName.substring(0, logFileName.lastIndexOf('.')) + "-test.log";
	 * 
	 * File logFile = new File(logFileName); File renamedFile = new
	 * File(logFile.getAbsoluteFile()+"."+System.currentTimeMillis());
	 * 
	 * if (logFile.exists()) { if (!logFile.renameTo(renamedFile))
	 * System.err.println("Could not rename log file!"); }
	 * 
	 * fAppender.setFile(logFile.getAbsolutePath());
	 * fAppender.activateOptions(); }
	 */

}
